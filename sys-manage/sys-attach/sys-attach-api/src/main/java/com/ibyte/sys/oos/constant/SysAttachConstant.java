package com.ibyte.sys.oos.constant;

/**
 * 附件机制常量
 * @author <a href="mailto:shangzhi.ibyte@gmail.com">iByte</a>
 * @since 1.0.1
 */
public interface SysAttachConstant {

    /**
     * 默认的存储实现
     */
    String SYS_ATTACH_DEFAULT_STORE = "sys.attach.store.currentStoreLocation";

    /**
     * 默认的加密方式
     */
    String SYS_ATTACH_DEFAULT_ENCRY = "sys.attach.store.encry.currentEncryMethod";
}
